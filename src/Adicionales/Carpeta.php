<?php


namespace HynoTech\UsosGenerales\Adicionales;


class Carpeta {
	public $id;
	public $nombre;
	public $fechaCreacion;
	public $href;
	public $linkKey;
	public $linkSecureHash;
	public $linkSubPath;
	public $linkType;
	public $propietario;
	public $subCarpetas;
	public $archivos;
	public $dataOriginal;
}
