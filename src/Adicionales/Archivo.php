<?php


namespace HynoTech\UsosGenerales\Adicionales;


class Archivo {
	public $id;
	public $nombre;
	public $href;
	public $peso;
	public $fechaCreacion;
	public $fechaEdicion;
	public $miniatura;
	public $tipo;
	public $icono;
	public $previsualizacion;
	public $dataOriginal;
}
